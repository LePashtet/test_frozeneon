import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    content: [],
    loading: false,
    dialog: null
  },
  mutations: {
    SET_CONTENT: (state, payload) => {
      state.content = payload;
    },
    SET_LOADING: (state, payload) => {
      state.loading = payload;
    },
    SET_DIALOG: (state, payload) => {
      state.dialog = payload;
    }
  },
  actions: {
    async search({ commit }, params) {
      commit("SET_LOADING", true);
      try {
        const { data } = await axios.get(
          "https://registry.npmjs.org/-/v1/search",
          {
            params
          }
        );
        commit("SET_CONTENT", data.objects);
      } catch (e) {
        console.log(e);
      } finally {
        commit("SET_LOADING", false);
      }
    }
  },
  modules: {}
});
